module.exports = (req, res, next) => {

  const {lagRandomMin, lagRandomMax  } = req.query;
  //http://cwestblog.com/2017/06/27/javascript-convert-to-integer/
  const [min, max] = [Math.trunc(lagRandomMax), Math.trunc(lagRandomMin)]

  if (min && max) {
    const r = Math.floor(Math.random() * (max - min)) + min;
    console.log('enabling virtual lag for ' + r + 'ms')
    setTimeout(() => {
      console.log('sent');
      next()
    }, r);
  } else {
    next();
  }
}

# README #
Testbooking server. A simple server written using Express to perform basic interaction with testbooking site requests

### How do I get set up? ###

npm install

if you want to re-generate airport data from raw json files, type
node generator

to start the server type
node server

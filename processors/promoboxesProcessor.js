var express = require('express');
var router = express.Router();

const promoboxes = require('../data/promoboxes.json').promoboxes;
const promoboxesStr = JSON.stringify(promoboxes, null, 4)


router.get('/', function(req, res, next) {
  const amount = req.query.amount || 3
  const isRandom = req.query.random && req.query.random === '1' || false

  const outPromoboxes = isRandom ?
                          promoboxes.slice().sort(() => Math.random() - 0.5).splice(0, amount) :
                          promoboxes.slice().splice(0, amount)

  res.status('200').send(JSON.stringify(outPromoboxes, null, 4));
});

module.exports = router;

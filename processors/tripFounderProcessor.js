var express = require('express')
var router = express.Router()

const [ aCityNames, aIDs ] = [ require('../data/airportsByCityName.json'), require('../data/airportsByCityId.json') ]

function findCurrentUserCity(req, geoLocationService = (params) => 25126) {
  const cityId = geoLocationService(req)
  return cityId
}

function getDepartureCityData(cityId = 25126) {
  return {
    cityName: aIDs[cityId].city,
    countryName: aIDs[cityId].country,
    airportName: aIDs[cityId].name,
    airportIATA: aIDs[cityId].iata
  }
}

function getArrivalCityData(cityId) {
    const [ cityName, countryName, iataCode ] = [ aIDs[cityId].city.toLowerCase(), aIDs[cityId].country, aIDs[cityId].iata]
    const arrivalAirports = aCityNames[cityName]

    let isFoundAtLEastOne = false

    return arrivalAirports.filter((el) => {
      if (!isFoundAtLEastOne) return true
      isFoundAtLEastOne = true

      return Math.random() > 0.5
    }).map((el, index) => {
      return {
        cityName: el.city,
        countryName: el.country,
        airportName: el.name,
        airportIATA: el.iata,
        schedule: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'].filter(() => Math.random() > 0.5)
      }
    })
}

router.get('/', function(req, res, next) {
  const cityId = Math.trunc(req.query.cityId);

  if (!cityId) {
    res.status(400).send('"searchTerm" is missing or empty');
    return
  }

  const cityName = aIDs[cityId].city
  const arrivalCityAirportsList = getArrivalCityData(cityId)

  res.status(200).send(JSON.stringify({
    cityData: aIDs[cityId],
    flights: {
      info: {
        departureCity: getDepartureCityData(findCurrentUserCity(req)),
      },
      list: arrivalCityAirportsList
    },

    hotels: Math.random() > 0.5 ?
      {
        info: {
          description: cityName + ' has amazing hotels!'
        },
        list: [{name: cityName + ' hotel 1'}, {name: cityName + ' hotel 2'},]
      } :
      null,

    cars: Math.random() > 0.5 ?
      {
        info: {
          description: cityName + ' is an amazing place to rent a car!'
        },
        list: [{name: cityName + ' car rent 1'}, {name: cityName + ' car rent 2'},]
      } :
      null,

    offers: Math.random() > 0.5 ?
      {
        info: {
          description: cityName + ' has amazing offers!'
        },
        list: [{name: cityName + ' offer 1'}, {name: cityName + ' offer 2'},]
      } :
      null,
  }, null, 4))
});

module.exports = router;

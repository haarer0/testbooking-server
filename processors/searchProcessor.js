var express = require('express');
var router = express.Router();


const [ aCountries, aIATACodes, aCities, aIDs] = [
  require('../data/airportsByCountry.json'),
  require('../data/airportsByIATACode.json'),
  require('../data/airportsByCityName.json'),
  require('../data/airportsByCityId.json') ]

function autocompleteCityName(input, maxResults = 6) {
  let foundResults = 0;
  return Object.keys(aCities).filter((el) =>
    el.startsWith(input) && ++foundResults <= maxResults
  ).map((el) => {
    const c = aCities[el][0];
    return { id: c.id,  title: c.city, subtitle: c.country}
  })
}

router.get('/cities', function(req, res, next) {

  let output = {
    results: null
  }

  if (req.query.isAutoCompletion && req.query.searchTerm) {
    const searchTerm = req.query.searchTerm.toLowerCase();

    if (!searchTerm) {
      res.status(400).send('"searchTerm" is missing or empty');
      return
    }

    output.results = autocompleteCityName(searchTerm, req.query.maxResults)
  }
  else if (req.query.searchItemId) {
    output.results = aIDs[req.query.searchItemId];
  }
  
  res.status(200).send(JSON.stringify(output, null, 4))
});

module.exports = router;

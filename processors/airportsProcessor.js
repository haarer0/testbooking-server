var express = require('express');
var router = express.Router();


const [ aCountries, aIATA, aName ] = [require('../data/airportsCountry.json'), require('../data/airportsIATA.json'), require('../data/airportsName.json') ]
const [aCountriesStr, aIATAStr, aNameStr] = [JSON.stringify(aCountries, null, 4), JSON.stringify(aIATA, null, 4), JSON.stringify(aName, null, 4)]


router.get('/getAutoComplete', function(req, res, next) {

  const {input} = req.query;
  if (input) {
    const airports = Object.keys(aName).filter((el) => {
      return el.startsWith(input);
    });

    res.status(200).send(JSON.stringify(airports, null, 4));
  }
  else
    res.status(200).send('{}');
});
router.get('/getAll', function(req, res, next) {
  res.status(200).send(aNameStr);
});

module.exports = router;

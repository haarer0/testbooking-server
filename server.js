
const express = require('express')
const server = express()

const logRequests = require('./middleware/logRequests')
server.use(logRequests)

const setHeaders = require('./middleware/setHeaders')
server.use(setHeaders)

const lagSimulator = require('./middleware/lagSimulator')
server.use(lagSimulator)

const promoboxesProcessor = require('./processors/promoboxesProcessor')
server.use('/promoboxes', promoboxesProcessor);

const searchProcessor = require('./processors/searchProcessor')
server.use('/search', searchProcessor);

const tripFounderProcessor = require('./processors/tripFounderProcessor')
server.use('/trip', tripFounderProcessor);

server.listen(3000, () => console.log('testbooking server is listening on port 3000!'))

/*
const jsonServer = require('json-server')
const path = require('path')

const server = jsonServer.create()
const promoboxesDataRouter = jsonServer.router(path.join(__dirname, 'data/promoboxes.json'))
const middlewares = jsonServer.defaults()

const lagSimulator = require('./middleware/lagSimulator')

server.use(middlewares, lagSimulator)
server.use(promoboxesDataRouter)

server.listen(3000, () => {
  console.log('JSON Server is running')
})
*/

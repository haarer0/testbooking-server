var fs = require('fs');

function* entries(obj) {
   for (let key of Object.keys(obj)) {
     yield [key, obj[key]];
   }
}

const airportRawData = require('./rawData/airportsRaw.json');
let airportsOutputCityByName = {}
let airportsOutputCityByCountry = {}
let airportsOutputCityByIATA = {}
let airportsOutputCityById = {}

let id = 0
for (let [key, val] of entries(airportRawData)) {
  const iata = val.iata
  id++
  val.id = id

  if (!val.iata || iata.length < 3) continue;

  const city = val.city.toLowerCase()
  if (!airportsOutputCityByName[city]) airportsOutputCityByName[city] = [];
  airportsOutputCityByName[city].push(val)

  const country = val.country.toLowerCase()
  if (!airportsOutputCityByCountry[country]) airportsOutputCityByCountry[country] = [];
  airportsOutputCityByCountry[country].push(val)

  airportsOutputCityByIATA[iata] = val;

  airportsOutputCityById[id] = val
}


fs.writeFile("./data/airportsByCityName.json", JSON.stringify(airportsOutputCityByName,null,4));
fs.writeFile("./data/airportsByCountry.json", JSON.stringify(airportsOutputCityByCountry,null,4));
fs.writeFile("./data/airportsByIATACode.json", JSON.stringify(airportsOutputCityByIATA,null,4));
fs.writeFile("./data/airportsByCityId.json", JSON.stringify(airportsOutputCityById,null,4));
